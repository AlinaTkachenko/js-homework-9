/*1. Створення нового HTML тегу на сторінці - спочатку const newTeg = document.createElement("div"); у дужках можна написати інший тег, який нам потрібен.
Потім треба вставити цей елемент на сторінку 
document.body.append(newTeg),
document.body.prepend(newTeg),
document.body.before(newTeg),
document.body.after(newTeg),
document.body.replaceWith(newTeg)
замість document.body ми можемо вказати інше місце куди треба вставити новий тег.*/
/*2. Перший параметр це кодове слово, яке вказує куди вставляти відносно elem. Його значенння має бути одним з наступних:
"beforebegin" – вставити html безпосередньо перед elem,
"afterbegin" – вставити html в elem, на початку,
"beforeend" – вставити html в elem, в кінці,
"afterend" – вставити html безпосередньо після elem.*/
/*3. Для видалення - node.remove()*/

arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function createList(arr, place = document.body) {
   const list = document.createElement('ul');
   list.innerText = 'Список:';
   place.prepend(list);
   arr.forEach(function (element) {
      const item = document.createElement('li');
      item.innerText = `- ${element}`;
      list.append(item);
   });
}

createList(arr);

